/*
 * Class Graph
 */
package project.horizontalbarpitchspaceoffset;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import static jm.constants.ProgramChanges.*;
import jm.music.data.*;
import jm.util.Play;

/*

The following class can be considered the main class. Here we use the array composed before to create a graph.
the x-axis is divided into quarters
the y-axis is divided by the notes present in the melody
All this part is inside the jPanel and there are two tools for moving us in the melody 
- jScroll 
- A variable in the form of a comboBox that is used for changing the dimension of the quarter (the distance between the notes)

In the class we try also to save the melody inside jMusic for playing it.
There is the possibility to choose different instruments.
 */
public class Graph extends JPanel {

    private String title;
    private ArrayList<MyChord> melody;
    private static Part part;
    private static int instrument;
    private static int stack = 100;
    private static int WIDTH_PANEL;
    private static int HEIGHT_PANEL = 300;

    // all the notes in the garph are rectangles with round corners
    // this array List tracks all the rectangles 
    private static ArrayList<MyRectangle> shapes = new ArrayList<>();

    // creation of a new Interface
    public static void CreateGui(String title, ArrayList melody, int stack) {

        // JPanel
        Graph mainPanel = new Graph(title, melody, stack);
        mainPanel.addMouseListener(new OpenNoteMouseListener(shapes));

        mainPanel.setPreferredSize(new Dimension(WIDTH_PANEL, HEIGHT_PANEL));
        // Creation of a new Frame
        JFrame frame = new JFrame(title);
        // jPanel is inside a JScroll
        final JScrollPane scroll = new JScrollPane(mainPanel);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);    // scroll only horizontal

        frame.setLayout(new BorderLayout());
        frame.add(scroll, BorderLayout.CENTER);

        // button located in the South part of the window for playing the melody
        JButton playMelody = new JButton("Play");
        playMelody.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // if play is pressed, play the melody.
                if (playMelody.getText().equals("Play")) {
                    playMelody.setText("Stop");
                    Play.midi(part);

                } else {
                    // if stop is pressed, stop the melody.
                    playMelody.setText("Play");
                    try {
                        Play.stopMidi();
                    } catch (StackOverflowError stackError) {
                        System.out.println("Audio Stopped");
                    }
                }

            }
        });
        frame.add(playMelody, BorderLayout.SOUTH);

        // combobox -> select instrument
        JComboBox combo = new JComboBox();
        combo.addItem("PIANO");
        combo.addItem("ACOUSTIC_GUITAR");
        combo.addItem("BANJO");
        combo.addItem("BASS");
        combo.addItem("POLYSYNTH");
        combo.addItem("SPACE_VOICE");
        combo.addItem("XYLOPHONE");

        combo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String name = (String) ((JComboBox) e.getSource()).getSelectedItem();
                switch (name) {
                    case "PIANO":
                        instrument = PIANO;
                        break;
                    case "ACOUSTIC_GUITAR":
                        instrument = ACOUSTIC_GUITAR;
                        break;
                    case "BANJO":
                        instrument = BANJO;
                        break;
                    case "BASS":
                        instrument = BASS;
                        break;
                    case "POLYSYNTH":
                        instrument = POLYSYNTH;
                        break;
                    case "SPACE_VOICE":
                        instrument = SPACE_VOICE;
                        break;
                    case "XYLOPHONE":
                        instrument = XYLOPHONE;
                        break;
                }
                part.setInstrument(instrument);

            }
        });
        frame.add(combo, BorderLayout.WEST);

        // combobox -> select space between notes
        JComboBox selectStack = new JComboBox();
        selectStack.addItem(60);
        selectStack.addItem(80);
        selectStack.addItem(100);
        selectStack.addItem(120);
        selectStack.addItem(140);
        selectStack.addItem(160);
        selectStack.addItem(200);
        selectStack.addItem(300);

        selectStack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int val = (int) ((JComboBox) e.getSource()).getSelectedItem();
                // when an item of the window is pressed resize space between notes
                SwingUtilities.invokeLater(() -> {
                    Graph.CreateGui(title, melody, val);
                });
                frame.dispose();
                try {
                    Play.stopMidi();
                } catch (StackOverflowError stackError) {
                    System.out.println("Audio Stopped");
                }
            }
        });
        frame.add(selectStack, BorderLayout.EAST);

        // window Listener, if the window is closed, stop also MIDI 
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                try {
                    Play.stopMidi();
                    frame.dispose();
                } catch (StackOverflowError stackError) {
                    System.out.println("Audio Stopped");
                }
            }
        });

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        // frame.getContentPane().add(mainPanel);

        frame.setVisible(true);

        frame.setSize(1000, 500);
        frame.setLocationRelativeTo(null);
        // frame.pack();

    }

    // constructor
    public Graph(String title, ArrayList melody, int stack) {
        this.title = title;
        this.stack = stack;
        this.melody = melody;
        convertMelodyIntoJMusic();
        retrieveInfo();
    }

    // the class takes the melody stored like MyMusic and transform it in a JMusic Part
    private void convertMelodyIntoJMusic() {
        // Part(String title, int instrument)
        part = new Part(title, PIANO);

        for (MyChord mychord : melody) {
            Phrase phrase = new Phrase(mychord.getNameEvent());
            double rhytm = mychord.getNumerator() * 4 / mychord.getDenominator();
            if (mychord.isMyChordRest()) {
                phrase.addRest(new Rest(rhytm));
            } else {
                ArrayList notes = mychord.getChord();
                int[] pitches = new int[notes.size()];
                for (int i = 0; i < notes.size(); i++) {
                    String name = ((MyNote) notes.get(i)).getNote();
                    int octave = ((MyNote) notes.get(i)).getOctave();
                    String accidental = ((MyNote) notes.get(i)).getAccidental();

                    int midi = NoteToMidiConverter.getMidiFromNote(name, octave, accidental);
                    pitches[i] = midi;
                }
                phrase.addChord(pitches, rhytm);
            }
            part.add(phrase);
        }
    }

    // the window horizontal size is created from the lenght of the melody  
    private void retrieveInfo() {
        double totalRhythm = 0.0;
        for (Phrase p : part.getPhraseArray()) {
            for (Note n : p.getNoteArray()) {
                totalRhythm += n.getRhythmValue();
            }
        }
        // WIDTH_PANEL
        WIDTH_PANEL = (int) ((stack + 1) * (totalRhythm / 4));
    }

    /////////////////////////////////////////// CREATION OF THE GRAPH ///////////////////////////////////
    // variables for graph 
    private final int labelPadding = 20;
    private final int padding = 12;
    private final int pointWidth = 5;
    private final Color gridColor = new Color(200, 200, 200, 200);
    private final Color rectangleColor = Color.GRAY;

    Map<Integer, Integer> allHeights = new HashMap<>();

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // draw backGround
        g2.setColor(Color.WHITE);
        g2.fillRect(padding + labelPadding, padding, getWidth() - (2 * padding) - labelPadding, getHeight() - 2 * padding - labelPadding);

        // draw horizontal dividing lines 
        g2.setColor(Color.BLUE);
        int numberYDivisions = (part.getHighestPitch() - part.getLowestPitch())+1;

        for (int i = 0; i < numberYDivisions; i++) {
            int x0 = padding + labelPadding;
            int x1 = pointWidth + padding + labelPadding;
            int y0 = getHeight() - ((i * (getHeight() - padding * 2 - labelPadding)) / numberYDivisions + padding + labelPadding);
            int y1 = y0;

            allHeights.put(i + part.getLowestPitch(), y0);

            // thin line
            g2.setColor(gridColor);
            g2.drawLine(padding + labelPadding + 1 + pointWidth, y0, getWidth() - padding, y1);

            // note names
            g2.setColor(Color.BLACK);
            String yLabel = NoteToMidiConverter.getNoteFromMidi(part.getLowestPitch() + i);
            FontMetrics metrics = g2.getFontMetrics();
            int labelWidth = metrics.stringWidth(yLabel);
            int sH = ((getHeight() - padding * 2 - labelPadding) / numberYDivisions) / 2;
            g2.drawString(yLabel, x0 - labelWidth - 6, y0 + (metrics.getHeight() / 2) - sH);

            // heavy lines 
            g2.drawLine(x0, y0, x1, y1);
        }
        // draw vertical dividing lines 
        // The goal is to divide the x-axis into quarters of time
        // the space bwtween quarters depends from the stack variable
        for (int j = 0; j <= (getWidth() - 2 * padding - labelPadding); j += stack) {
            int x0 = padding + labelPadding + j;
            int x1 = x0;
            int y0 = getHeight() - padding - labelPadding;
            int y1 = y0 - pointWidth;

            g2.setColor(gridColor);
            g2.drawLine(x0, getHeight() - padding - labelPadding - 1 - pointWidth, x1, padding);

            g2.setColor(Color.BLACK);
            String xLabel = (j / stack) * 4 + "";
            FontMetrics metrics = g2.getFontMetrics();
            int labelWidth = metrics.stringWidth(xLabel);
            g2.drawString(xLabel, x0 - labelWidth / 2, y0 + metrics.getHeight() + 3);

            g2.drawLine(x0, y0, x1, y1);
        }

        // horizontal and vertical lines of the axes need to be heavy
        g2.drawLine(padding + labelPadding, getHeight() - padding - labelPadding, padding + labelPadding, padding);
        g2.drawLine(padding + labelPadding, getHeight() - padding - labelPadding, getWidth()
                - padding, getHeight() - padding - labelPadding);

        g2.setColor(rectangleColor);

        // Rectangles for the notes
        /*
            HOW IT WORKS
        For drawing the notes we draw rectangles, in particular we get the size of a rectangle from his duration 
        Then, we add the lenght to a variable "arrived" that represents where we are arrived.
        We start drawing always from "arrived"
         */
        double arrived = labelPadding + padding;
        shapes.clear();
        for (Phrase p : part.getPhraseArray()) {
            double[] rithmArray = p.getRhythmArray();
            // the rythm is on the last note 
            double rythm = rithmArray[p.getRhythmArray().length - 1];
            double x = (rythm / 4) * stack;

            for (Note n : p.getNoteArray()) {
                if (n.getPitch() != Integer.MIN_VALUE && allHeights.containsKey(n.getPitch())) {
                    // inside of a note 
                    int w = (int) (arrived);
                    int h = allHeights.get(n.getPitch());

                    // System.out.println("Larghezza = " + w + " altezza = " + h);
                    int WIDTH = (int) x;
                    int HEIGHT = (getHeight() - padding * 2 - labelPadding) / numberYDivisions;
                    Rectangle r = new Rectangle();
                    r.x = w;
                    r.y = h - HEIGHT;
                    r.width = WIDTH;
                    r.height = HEIGHT;

                    g2.setColor(rectangleColor);
                    g2.fillRoundRect(w, h - HEIGHT, WIDTH, HEIGHT, 8, 8);
                    g2.setColor(Color.BLACK);
                    g2.drawRoundRect(w, h - HEIGHT, WIDTH, HEIGHT, 8, 8);
                    // we save the Note inside the array of rectangles for adding an eventListener
                    shapes.add(new MyRectangle(r, n.getPitch(), rythm, false, p.getTitle()));
                } else {
                    // inside of a rest 
                    shapes.add(new MyRectangle(new Rectangle(), Integer.MIN_VALUE, rythm, true, p.getTitle()));
                }
            }
            arrived += x;
        }

    }

}
