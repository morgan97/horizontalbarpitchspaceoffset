/*
 PROGETTO DI PROGRAMMAZIONE PER LA MUSICA 
    
    Il seguente progetto si propone la replica di una delle funzione di music21
    http://web.mit.edu/music21/ . 
    La funzione è HorizontalBarPitchSpaceOffset; in particolare essa permette, a partire da un file 
    un file .mxl, di realizzare un grafico che mostra:
    asse y -> le note presenti nella melodia 
    asse x -> la loro ripetizione per ordine di tempo 

    La replica sarà realizzata usando lo standard IEEE 1599 .xml 
    IEEE 1599 è un linguaggio per una descrizione completa della musica standardizzato 
    dalla IEEE Standards Association nel 2008.
    
 */
package project.horizontalbarpitchspaceoffset;

import java.awt.EventQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Morgan Malavasi
 */

public class ProjectHorizontalBarPitchSpaceOffset {

    public static void main(String[] args) {
        // if Nimbus is present, use it
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()){
                if ("Nimbus".equals(info.getName())){
                    UIManager.setLookAndFeel(info.getClassName());
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ProjectHorizontalBarPitchSpaceOffset.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // invoke new Runnable
        EventQueue.invokeLater(new Runnable(){
            @Override
            public void run() {
                new IEEEGraph().setVisible(true);
            }
        });
        
    }
    
}
