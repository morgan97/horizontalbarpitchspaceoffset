/*
 * Class NoteToMidiConverter
 */
package project.horizontalbarpitchspaceoffset;

/**
 *
 * @author morgan
 */
import java.math.BigDecimal;
import java.math.BigInteger;

public class NoteToMidiConverter {
    // following class contains switchers from name of the note in latin to MIDI e viceversa.
    
    /**
     *
     * @param pitch
     * @param octave
     * @param accidental
     * @return
     */
    public static int getMidiFromNote(String pitch, int octave, String accidental) {
        int res = -1;

        // creation of the note with octave
        String name;
        switch (accidental) {
            case "sharp":
                name = pitch + "#";
                break;
            case "flat":
                name = pitch + "b";
                break;
            default:
                name = pitch;
                break;
        }

        switch (name) {
            case "C":
                res = 0;
                break;
            case "C#":
                res = 1;
                break;
            case "Db":
                res = 1;
                break;
            case "D":
                res = 2;
                break;
            case "D#":
                res = 3;
                break;
            case "Eb":
                res = 3;
                break;
            case "E":
                res = 4;
                break;
            case "F":
                res = 5;
                break;
            case "F#":
                res = 6;
                break;
            case "Gb":
                res = 6;
                break;
            case "G":
                res = 7;
                break;
            case "G#":
                res = 8;
                break;
            case "Ab":
                res = 8;
                break;
            case "A":
                res = 9;
                break;
            case "A#":
                res = 10;
                break;
            case "Bb":
                res = 10;
                break;
            case "B":
                res = 11;
                break;
            default:
                break;
        }

        res += octave * 12;

        return res;
    }

    public static String getNoteFromMidi(int midi) {
        int rest = midi % 12;
        int octave = midi / 12;
        String res;
        switch (rest) {
            case 0:
                res = "C";
                break;
            case 1:
                res = "C#";
                break;
            case 2:
                res = "D";
                break;
            case 3:
                res = "D#";
                break;
            case 4:
                res = "E";
                break;
            case 5:
                res = "F";
                break;
            case 6:
                res = "F#";
                break;
            case 7:
                res = "G";
                break;
            case 8:
                res = "G#";
                break;
            case 9:
                res = "A";
                break;
            case 10:
                res = "A#";
                break;
            case 11:
                res = "B";
                break;
            default:
                res = "C";
                break;
        }
        return res + octave;
    }

    public static String getLatinNoteFromMidi(int midi) {
        int rest = midi % 12;
        String res;
        switch (rest) {
            case 0:
                res = "DO";
                break;
            case 1:
                res = "DO#";
                break;
            case 2:
                res = "RE";
                break;
            case 3:
                res = "RE#";
                break;
            case 4:
                res = "MI";
                break;
            case 5:
                res = "FA";
                break;
            case 6:
                res = "FA#";
                break;
            case 7:
                res = "SOL";
                break;
            case 8:
                res = "SOL#";
                break;
            case 9:
                res = "LA";
                break;
            case 10:
                res = "LA#";
                break;
            case 11:
                res = "SI";
                break;
            default:
                res = "DO";
                break;
        }
        return res;
    }
     
    // for getting the tempo of a note
    /**
     *
     * @param time
     * @return
     */
    public static String getTempo(double time) {
        try {
            String input = "" + (time/4);
            int[] result = convertToFraction(input);

            return result[0] + "/" + result[1];
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    // following code taken from https://codippa.com/how-to-convert-a-decimal-number-into-a-fraction-in-java/
    static private int[] convertToFraction(String numberStr) throws Exception {
        String[] parts = null;
        try {
            //parse the string to convert to a number
            BigDecimal number = new BigDecimal(numberStr);
            parts = number.toString().split("\\.");
            //check if there was a decimal in the number
            if (parts.length < 2) {
                throw new ArrayIndexOutOfBoundsException("Error: Please ensure that"
                        + " the entered value has a decimal.");
            }
        } catch (NumberFormatException e) {
            throw new Exception(
                    "Error: Please enter the number in proper format.");
        } catch (ArrayIndexOutOfBoundsException ae) {
            throw ae;
        }
        /*
	  * Remove decimal
         */

        // determine the denominator
        BigDecimal den = BigDecimal.TEN.pow(parts[1].length());
        // determine the numerator
        BigDecimal num = (new BigDecimal(parts[0]).multiply(den))
                .add(new BigDecimal(parts[1]));
        return reduceFraction(num.intValue(), den.intValue());
    }

    /**
     * Convert the numerator and denominator to their minimum values where no
     * further cancellations are possible. For this we need to determine the
     * common divisor of numerator and denominator elements
     *
     * @param num
     * @param den
     * @return
     */
    static private int[] reduceFraction(int num, int den) {
        // get the gcd
        int gcd = BigInteger.valueOf(num).gcd(BigInteger.valueOf(den))
                .intValue();
        // divide the numerator and denominator by gcd
        int[] fractionElements = {num / gcd, den / gcd};
        return fractionElements;
    }

}
