/*
 * Class MySound
 */
package project.horizontalbarpitchspaceoffset;

import java.util.ArrayList;
import javax.swing.SwingUtilities;

/**
 *
 * @author morgan
 */
public class MySound {
    
    // MySound represents the entire melody played by an instrument or simply like a part of the file 
    private ArrayList<MyChord> music = new ArrayList<>();
    private String title = "";

    // empty constructor
    public MySound() {
    }
    
    // constructor with completed array in input 
    public MySound(ArrayList<MyChord> list) {
        this.music = list;
    }

    // get melody
    public ArrayList getMusic() {
        return music;
    }

    // set tille of the melody
    public void setTitle(String title) {
        this.title = title;
    }

    
    // insert in the array list a new myChord
    public void insertNewChordOrRest(MyChord myChord) {
        music.add(myChord);
    }

    // launch graph
    public void launchGraph() {
        SwingUtilities.invokeLater(() -> {
            Graph.CreateGui(title, music, 70);
        });
    }
    

}
