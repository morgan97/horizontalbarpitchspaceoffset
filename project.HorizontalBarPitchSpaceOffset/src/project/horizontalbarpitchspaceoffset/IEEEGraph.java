/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.horizontalbarpitchspaceoffset;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author morgan
 */
public class IEEEGraph extends JFrame {
    
    // size of the window
    private final int FRAME_WIDTH = 400;
    private final int FRAME_HEIGHT = 250;

    private JLabel titolApplication;
    private JLabel description;
    private JButton button;
    private JComboBox comboBox;
    private ActionListener l;
    Map<Integer, String> partNames = new HashMap<>();
    
    // init application
    public IEEEGraph() {
        super("IEEEGraph");
        initComponents();
    }

    // init window and components
    private void initComponents() {

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.CENTER, 200, 20));
        setLocationRelativeTo(null);
        setResizable(false);
        getContentPane().setBackground(Color.WHITE);
        comboBox = new JComboBox();

        titolApplication = new JLabel("Visualizzatore di note");
        titolApplication.setFont(new Font("Arial", Font.BOLD, 20));

        description = new JLabel("Carica un file ieee1599.xml ");
        description.setFont(new Font("Arial", Font.ITALIC, 15));

        // button for opening a JFileChooser 
        button = new JButton("Carica");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoadFile(e);
                // openFile(e);
            }

        });

        add(titolApplication);
        add(description);
        add(button);
        add(comboBox);
    }

    // open JFileChooser
    private void LoadFile(ActionEvent e) {
        // if there is something in the combobox, remove it
        comboBox.removeActionListener(l);
        comboBox.removeAllItems();
        
        // select file with format = .xml
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("IEEE1599", "xml"));

        // if the evaluation is correct continue
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            // operation on data
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder;
            try {
                docBuilder = docFactory.newDocumentBuilder();
                Document myXml = docBuilder.parse(chooser.getSelectedFile().getPath());

                XPathFactory myXPathFactory = XPathFactory.newInstance();
                XPath xPath = myXPathFactory.newXPath();
                
                // try to discover the number of parts in the file
                NodeList currentPart = (NodeList) (xPath.evaluate("/ieee1599/logic/los/part", myXml, XPathConstants.NODESET));
                exitBecauseSizeEqualZero(currentPart.getLength());      // if size == 0 exit because the file has no parts
                
                // add to the combobox all the parts found in the file from the id
                for (int i = 0; i < currentPart.getLength(); i++) {
                    String name = ((Element) (currentPart.item(i))).getAttribute("id");
                    partNames.put(i, name);
                }

                // enter data -> comboBox
                for (int i = 0; i < currentPart.getLength(); i++) {
                    comboBox.addItem("" + partNames.get(i));
                }
                
                // actionListener -> combobox
                // when an item is pressed launch the function that particoular part
                comboBox.addActionListener(l = new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            String t = (String) ((JComboBox) e.getSource()).getSelectedItem();
                            openFile(e, t, chooser.getSelectedFile().getPath());
                        } catch (SAXException | IOException ex) {
                            Logger.getLogger(IEEEGraph.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                

            } catch (ParserConfigurationException | IOException | XPathExpressionException ex) {
                Logger.getLogger(IEEEGraph.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                // if the file isn't a ieee1599 retry loading new file 
                System.out.println("The following file is not correct retry...");
                LoadFile(e);
            }

        }

    }

    /*
    Open file with JFileChooser
     */
    private void openFile(ActionEvent e, String namePart, String Path) throws SAXException, IOException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
            Document myXml = docBuilder.parse(Path);

            XPathFactory myXPathFactory = XPathFactory.newInstance();
            XPath xPath = myXPathFactory.newXPath();

            // read again the content -> parts
            NodeList currentPart = (NodeList) (xPath.evaluate("/ieee1599/logic/los/part", myXml, XPathConstants.NODESET));

            for (int i = 0; i < currentPart.getLength(); i++) {

                /*
                        HOW IT WORKS 
                        The point of the following structures is to create a new melody for each part detected inside the xml file given in input 
                        for the namePart part there will be a call to a new class for the creation of the graph
                 */
                MySound mySound = new MySound();

                // get name of each part
                String name = ((Element) (currentPart.item(i))).getAttribute("id");

                // get measures 
                NodeList currentMeasures = null;

                // query to retrieve measures in the part
                String queryMeasure = "/ieee1599/logic/los/part[@id=\"" + name + "\"]/measure";
                currentMeasures = (NodeList) (xPath.evaluate(queryMeasure, myXml, XPathConstants.NODESET));

                // for all the measures of the part get chords and rests 
                if (currentMeasures != null) {
                    for (int j = 0; j < currentMeasures.getLength(); j++) {
                        String number = ((Element) (currentMeasures.item(j))).getAttribute("number");

                        // for each measure number find all the chords and the rests
                        NodeList currentChordRest = null;
                        String queryChordRest = "/ieee1599/logic/los/part[@id=\"" + name + "\"]/measure[@number=\"" + number + "\"]/voice/*";
                        // all the chords are now inside currentChordRest variable
                        currentChordRest = (NodeList) (xPath.evaluate(queryChordRest, myXml, XPathConstants.NODESET));

                        // System.out.println("measure number = " + number + " has " + currentChordRest.getLength() + " children");
                        // for all the chord and rest of the measure number ...
                        if (currentChordRest != null) {
                            for (int k = 0; k < currentChordRest.getLength(); k++) {

                                MyChord myChord = null;
                                // myChord can be a chord or a rest. If rest it has null value for the notes.
                                Node now = currentChordRest.item(k);
                                Element n = (Element) now;
                                String nameChordRest = n.getNodeName();
                                if (nameChordRest.equals("chord")) {
                                    // if we're in a chord take octave - pitch - accidental for each note  
                                    String nameEvent = n.getAttribute("event_ref");

                                    // duration 
                                    String queryChordDuration = "/ieee1599/logic/los/part[@id=\"" + name + "\"]/measure[@number=\"" + number + "\"]/voice/chord[@event_ref=\"" + nameEvent + "\"]/duration";
                                    Node duration = (Node) xPath.evaluate(queryChordDuration, myXml, XPathConstants.NODE);
                                    int numerator = Integer.parseInt(((Element) (duration)).getAttribute("num"));
                                    int denominator = Integer.parseInt(((Element) (duration)).getAttribute("den"));

                                    // pitch 
                                    String queryChordPitch = "/ieee1599/logic/los/part[@id=\"" + name + "\"]/measure[@number=\"" + number + "\"]/voice/chord[@event_ref=\"" + nameEvent + "\"]/notehead/pitch";
                                    NodeList pitch = (NodeList) xPath.evaluate(queryChordPitch, myXml, XPathConstants.NODESET);

                                    ArrayList<MyNote> mynotes = new ArrayList<>();

                                    for (int z = 0; z < pitch.getLength(); z++) {
                                        // get all the element of the note and create new element 
                                        int octave = Integer.parseInt(((Element) (pitch.item(z))).getAttribute("octave"));
                                        String step = (((Element) (pitch.item(z))).getAttribute("step"));
                                        String accidental = (((Element) (pitch.item(z))).getAttribute("actual_accidental"));
                                        MyNote actualCompleteNote = new MyNote(octave, step, accidental);
                                        mynotes.add(actualCompleteNote);
                                    }

                                    // once we obtained the duration and the pitch info we create the put the chord in myChord
                                    myChord = new MyChord(numerator, denominator, mynotes, nameEvent, false);

                                }
                                if (nameChordRest.equals("rest")) {
                                    // if we're in a chord take just duration of the pause and set to true variable isRest in MyChord
                                    String nameEvent = n.getAttribute("event_ref");

                                    // duration 
                                    String queryRestDuration = "/ieee1599/logic/los/part[@id=\"" + name + "\"]/measure[@number=\"" + number + "\"]/voice/rest[@event_ref=\"" + nameEvent + "\"]/duration";
                                    Node duration = (Node) xPath.evaluate(queryRestDuration, myXml, XPathConstants.NODE);
                                    int numerator = Integer.parseInt(((Element) (duration)).getAttribute("num"));
                                    int denominator = Integer.parseInt(((Element) (duration)).getAttribute("den"));

                                    // once we obrained the duration and the pitch info we create the real chord 
                                    myChord = new MyChord(numerator, denominator, null, nameEvent, true);
                                }
                                if (myChord != null) {
                                    // in the end, if the chord isn't null insert the chord in the melody
                                    mySound.insertNewChordOrRest(myChord);
                                }
                            }
                        }

                    }
                }
                
                // once the process is finished, set the title of the melody and launch the graph for the one who has the namePart desidered by the user
                mySound.setTitle(name);
                if (name.equals(namePart)) {
                    mySound.launchGraph();
                }

            }

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(IEEEGraph.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(IEEEGraph.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Impossibile recuperare le parti");
        }

    }
    
    // if the size is minor or equal than zero, exit or retry 
    private void exitBecauseSizeEqualZero(int size){
        if (size <= 0){
            System.out.println("Impossible to retrieve data");
            System.exit(1);
        }
    }

}
